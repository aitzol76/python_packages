from setuptools import setup

setup(
        name="tools_package",
        version="1.0",
        description="Python tools package",
        author="Aitzol Berasategi",
        author_email="aitzol@aitzol.eus",
        url="aitzol.eus",
        packages=["tools_package", "tools_package.email_check"]
    
    )
